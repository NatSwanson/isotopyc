from isotopes import *

class Element:
    def __init__(self, name, isotopes, abundances):
        if (len(isotopes) != len(abundances)):
            raise ValueError("Must provide equal number of "
                             "elements and proportions.")
        if (np.abs(np.sum(abundances) - 1.) > 1e-3):
            raise ValueError("Abundances must sum to 1")
        self.name = name
        self.isotopes = isotopes
        self.abundances = abundances
        
        self.mass = 0
        for i in range(len(isotopes)):
            self.mass += self.isotopes[i].mass*abundances[i]

H = Element("hydrogen", [H1, H2], [0.999885, 0.000115])
C = Element("carbon", [C12, C13], [0.9893,0.0107])
N = Element("nitrogen", [N14, N15], [0.99632, 0.00368])
O = Element("oxygen", [O16, O17, O18], [0.99757, 0.00038, 0.00205])
F = Element("fluorine", [F19], [1.00])
Si = Element("silicon", [Si28, Si29, Si30], [0.92223, 0.04685, 0.03092])
S = Element("sulfur", [S32, S33, S34, S36], [0.9493, 0.0076, 0.0429, 0.0002])
Cr = Element("chromium", 
             [Cr50, Cr52, Cr53, Cr54], 
             [0.04345, 0.83789, 0.09501, 0.02365])
Ar = Element("argon", [Ar36, Ar38, Ar40], [0.003365, 0.000632, 0.996003])
Ti = Element("titanium", 
             [Ti46, Ti47, Ti48, Ti49, Ti50], 
             [0.0825, 0.0744, 0.7372, 0.0541, 0.0518])
Fe = Element("iron", 
             [Fe54, Fe56, Fe57, Fe58], 
             [0.05845, 0.91754, 0.02119, 0.00282])
Ni = Element("nickel", 
             [Ni58, Ni60, Ni61, Ni62, Ni64], 
             [0.680769, 0.262231, 0.011399, 0.036345, 0.009256])
Xe = Element("xenon", 
             [Xe124, Xe126, Xe128, Xe129, 
              Xe130, Xe131, Xe132, Xe134, Xe136],
             [9.52e-4, 8.90e-4, 0.019102, 0.264006, 
              0.040710, 0.212324, 0.269086, 0.104357, 0.088573])
