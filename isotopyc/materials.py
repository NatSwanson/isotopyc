from elements import *

class Material:
    # Based on geant4 Materials class

    def __init__(self, name, density, elements, 
                 num_atoms, mass_fractions, mass):
        N_A = 6.02e23
        self.name = name
        self.density = density
        self.elements = elements
        self.num_atoms = num_atoms
        self.mass_fractions = mass_fractions
        self.mass = mass
        self.number_density = (self.density/self.mass)*N_A
        
    @classmethod
    def byatomnum(cls, name, density, elements, num_atoms):
        if (len(elements) != len(num_atoms)):
            raise ValueError("Must provide equal number of "
                             "elements and atom numbers.")
        mass = 0
        mass_fractions = []
        for i in range(len(elements)):
            element_mass = elements[i].mass*num_atoms[i]
            mass_fractions.append(element_mass)
            mass += element_mass
        mass_fractions = np.array(mass_fractions)/mass
        return cls(name, density, elements, num_atoms, mass_fractions, mass)

    @classmethod
    def bymassfrac(cls, name, density, elements, mass_fractions):
        if (len(elements) != len(mass_fractions)):
            raise ValueError("Must provide equal number of 
                             "elements and mass fractions.")
        if (np.abs(np.sum(mass_fractions) - 1.) > 1e-3):
            raise ValueError("Mass fractions must sum to 1")
        mass = 0
        relative_mass = 0
        for i in range(len(elements)):
            relative_mass += elements[i].mass*mass_fractions[i]
        num_atoms = []
        for i in range(len(elements)):
            atom_num = ((mass_fractions[i]*relative_mass)/elements[i].mass)
            atom_num = int(round(atom_num + 0.5))
            mass += atom_num*elements[i].mass
            num_atoms.append(atom_num)
        return cls(name, density, elements, num_atoms, mass_fractions, mass)
    
    def get_number_densities(self):
        N_A = 6.02e23
        number_densities = []
        for i in range(len(self.elements)):
            number_densities.append(N_A*self.density 
                                    * self.mass_fractions[i]
                                    / self.elements[i].mass)
        return number_densities
        
air = Material.bymassfrac("air", 0.0012, 
                          [O, N, Ar], 
                          [0.20946, 0.7812, 0.009340])
steel = Material.bymassfrac("steel", 8.03, 
                 [C, S, Si, Cr, Ni, N, Fe], 
                 [0.0004, 0.00015, 0.0032, 0.19, 0.10, 0.005, 0.70125])
water = Material.byatomnum("water", 1, [H, O], [2,1])
reflector = Material.byatomnum("reflector", 1.29, [C, H], [2,6])
poly_foam = Material.byatomnum("foly foam", 0.032, [C, H], [1,2])
titanium = Material.byatomnum("titanium", 4.507, [Ti], [1])
teflon = Material.byatomnum("Teflon", 2.16, [C, F], [1,2])
LXe = Material.byatomnum("liquid xenon", 2.888, [Xe], [1])
