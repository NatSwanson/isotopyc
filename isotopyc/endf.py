import numpy as np
import numpy.matlib
import scipy.special
import scipy.interpolate
import matplotlib.pyplot as plt


def legendre(N,X) :
    """
    https://stackoverflow.com/questions/27061206/python-equivalent-of-matlabs-legendre-function
    """
    matrixReturn = np.zeros((N+1,X.shape[0]))
    for i in enumerate(X) :
        currValues = scipy.special.lpmn(N,N,i[1])
        matrixReturn[:,i[0]] = np.array([j[N] for j in currValues[0]])
    return matrixReturn

def get_reaction_xs(data_matrix, mt):
    mf = 3
    mf_index = 7
    mt_index = 8

    # Select the data block that matches the desired data and reaction type
    cut_lines = ((data_matrix[:,mf_index] == mf) 
                 * (data_matrix[:,mt_index] == mt))
    if (np.sum(cut_lines) == 0):
        print(f"MT={mt}, MF={mf} data not found")
        return None
    
    """
    Each section of data has a few lines of header info, which describes
    the number of data pairs and how those pairs are grouped for interpolation
    when plotting. 

    Each row has three pairs of data, so within a row, the grouping is
    x1 y1 x2 y2 x3 y3
    Manipulate the data to group all x values together and all y values together
    """
    start_line = 3
    data_matrix_short = data_matrix[cut_lines, :]
    num_pairs = data_matrix_short[1,5]
    num_lines = int(np.ceil(num_pairs/3))
    data_pairs = data_matrix_short[start_line:start_line+num_lines,0:6]
    data_pairs = data_pairs.T
    data_pairs = data_pairs.flatten("F")
    data_pairs = np.reshape(data_pairs, (2, len(data_pairs)//2), order="F")
    data_pairs = data_pairs.T

    return data_pairs
    
def get_ang_dist(data_matrix, mt):
    mf = 4
    mf_index = 7
    mt_index = 8   

    # Select the data block that matches the desired data and reaction type
    cut_lines = ((data_matrix[:,mf_index] == mf) 
                 * (data_matrix[:,mt_index] == mt))
    if (np.sum(cut_lines) == 0):
        print(f"MT={mt}, MF={mf} data not found")
        return None 
    data_matrix_short = data_matrix[cut_lines, :]

    data_table = \
    {
        "energy": [],
        "num_coeffs": [],
        "coeffs": []
    }

    start_line = 4
    flag_read_next_entry = True
    while (flag_read_next_entry):
        entry_energy = data_matrix_short[start_line, 1]
        entry_num_coeffs = int(data_matrix_short[start_line, 4])
        entry_num_lines = int(np.ceil(entry_num_coeffs/6))+1
        entry_lines_with_data = np.arange(1, entry_num_lines) \
                                + start_line
        entry_coeffs = data_matrix_short[entry_lines_with_data, 0:6].T
        entry_coeffs = np.reshape(entry_coeffs[0:entry_num_coeffs], 
                                  (1, entry_num_coeffs), order="F")

        data_table["energy"].append(entry_energy)
        data_table["num_coeffs"].append(entry_num_coeffs)
        data_table["coeffs"].append(entry_coeffs)        

        start_line += entry_num_lines
        if (start_line >= np.shape(data_matrix_short)[0]):
            flag_read_next_entry = False

    return data_table 

def read_endf(filename=None):
    if (filename is None):
        raise ValueError("Please provide a ENDF file to read")

    with open(filename, "r") as endf_file:
        flag_endf_search_data = True
        line_read = endf_file.readline()
        endf_current_line = 1

        while (flag_endf_search_data):
            line_read = endf_file.readline()
            endf_current_line += 1

            if (("1111 0  0" in line_read) or ("99999" in line_read) 
                or (line_read[71:75] == "1  0")):
                
                flag_endf_search_data = False

            if (line_read == ""):
                raise IOError("Reached end of ENDF file"
                              "without finding data marker. File may be broken")

        flag_read_next_line = True
        data_matrix = []
        line_index = 0
        column_sizes = [11, 11, 11, 11, 11, 11, 4, 2, 3]
        while (flag_read_next_line):
            data_matrix.append(np.zeros(len(column_sizes)))
            endf_line = endf_file.readline()            
            for column_num in range(len(column_sizes)):
                start = sum(column_sizes[0:column_num])
                end = start + column_sizes[column_num]
                data_point = endf_line[start:end]
                if (data_point != ""):
                    if ("+" in data_point):
                        data_point = data_point.replace("+", "e+")
                    elif ("-" in data_point):
                        data_point = data_point.replace("-", "e-")
                    try:
                        data_point = float(data_point)
                    except ValueError:
                        data_point = np.nan
                    data_matrix[line_index][column_num] = data_point
                else:
                    flag_read_next_line = False
            line_index += 1

    data_matrix = np.array(data_matrix)
    return data_matrix

def get_xs_vs_E(filename=None, neutron_energies_eV=[-1]):

    if (len(neutron_energies_eV) == 0):
        raise ValueError("Please provide an array of neutron energies")

    MT = 1
    data_matrix = read_endf(filename)
    data_pairs = get_reaction_xs(data_matrix, MT)
    
    if np.any(np.isnan(data_pairs)):
        print(f"Removing NaNs in cross-section data from {filename}")
        no_nan_cut = np.logical_not(np.isnan(data_pairs[:,0]))

        if (neutron_energies_eV == [-1]):
            E_min_log = int(np.log10(np.min(data_pairs[no_nan_cut,0])))
            E_max_log = int(np.log10(np.max(data_pairs[no_nan_cut,0])))
            neutron_energies_eV = np.logspace(E_min_log,
                                              E_max_log,
                                              10*(E_max_log - E_min_log + 1))
        xs_barns = np.interp(neutron_energies_eV, 
                             data_pairs[no_nan_cut,0], data_pairs[no_nan_cut,1])
    else:
        if (neutron_energies_eV == [-1]):
            E_min_log = int(np.log10(np.min(data_pairs[:,0])))
            E_max_log = int(np.log10(np.max(data_pairs[:,0])))
            neutron_energies_eV = np.logspace(E_min_log,
                                              E_max_log,
                                              10*(E_max_log - E_min_log + 1))
        xs_barns = np.interp(neutron_energies_eV, 
                             data_pairs[:,0], data_pairs[:,1])

    return xs_barns, neutron_energies_eV

def plot_xs_vs_E(energies, cross_sections):
    plt.figure()
    plt.plot(energies, cross_sections)
    plt.xscale("log")
    plt.yscale("log")
    plt.xlabel("Energy [eV]")
    plt.ylabel("Cross-Section [b]")
    plt.show()

def get_prob_vs_theta(filename=None, neutron_energy=0, cos_theta_list=[-1]):

    if (len(cos_theta_list) == 0):
        raise ValueError("Please provide an array of cos(theta) values.")

    if (cos_theta_list == [-1]):
        cos_theta_list = np.cos(np.linspace(0, np.pi, 100))

    MT = 2
    data_matrix = read_endf(filename)
    data_table = get_ang_dist(data_matrix, MT)

    #Pl_matrix is a matrix of Legendre polynomials    
    max_num_coeffs = max(data_table["num_coeffs"])

    Pl_matrix = np.zeros((max_num_coeffs+1, len(cos_theta_list)))
    for i in range(0, max_num_coeffs+1):
        Pl_cos_theta = legendre(i, cos_theta_list)
        Pl_matrix[i, :] = Pl_cos_theta[0,:]
    
    num_energies = len(data_table["energy"])
    func = np.zeros((num_energies, len(cos_theta_list)))
    for i in range(num_energies):
        num_coeffs = data_table["num_coeffs"][i]
        l_val_list = np.array([np.arange(num_coeffs+1)])
        l_val_multiplier = (2*l_val_list + 1)/2
        a_l = np.concatenate(([[1]], data_table["coeffs"][i]), axis=1)
        coeff = np.matlib.repmat(l_val_multiplier*a_l, len(cos_theta_list), 1)
        coeff = coeff.T
        func[i, :] = np.sum(coeff*Pl_matrix[0:num_coeffs+1,:],axis=0)

    interp_function = \
        scipy.interpolate.interp2d(data_table["energy"], cos_theta_list,
                                   func.T)
    probabilities = interp_function(neutron_energy, cos_theta_list)

    return probabilities, cos_theta_list

