import setuptools

with open("README.md", "r") as readme_file:
    readme = readme_file.read()

setuptools.setup(
    author="Will Taylor",
    author_email="wctaylor@librem.one",
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Science/Research",
        "Natural Language :: English",
        "Operating System :: OS Independent"
        "Programming Language :: Python :: 3",
        "Topic :: Scientific/Engineering :: Physics"
    ],
    description="Isotopic Info and Neutron Scattering Package",
    long_description=readme,
    long_description_content_type="text/markdown",
    keywords=["neutron", "isotope", "ENDF", "half-life"],
    name="isotopyc",
    packages=setuptools.find_packages(),
    python_requires=">=3.6",
    url="https://gitlab.com/wctaylor/isotopyc",
    version="0.0.1"
)

